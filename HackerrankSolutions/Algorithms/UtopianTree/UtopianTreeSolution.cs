﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerrankSolutions.Algorithms.UtopianTree
{
    public class UtopianTreeSolution
    {
        public static int UtopianTree(int n)
        {
            int result = 0;

            if (n == 0)
                return 1;

            if (n % 2 == 1)
            {
                n++;                
            }
            else
            {
                result++;
            }

            result += 2 * (1 - (int)Math.Pow(2, n / 2)) * -1;



            return result;
        }
    }
}
