﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerrankSolutions.Algorithms.ClimbingTheLeaderboard
{
    public class ClimbingTheLeaderboardSolution
    {
        private class RankedScoreComparer : IComparer<KeyValuePair<int, int>>
        {
            public int Compare(KeyValuePair<int, int> x, KeyValuePair<int, int> y)
            {
                return x.Key.CompareTo(y.Key);
            }
        }

        public static int[] ClimbingLeaderboard(int[] scores, int[] alice)
        {
            List<KeyValuePair<int, int>> rankedScores = GetRankedScores(scores);

            rankedScores.Sort((x, y) => x.Key.CompareTo(y.Key));

            int[] aliceRanks = new int[alice.Length];

            for (int i = 0; i < alice.Length; i++)
            {
                aliceRanks[i] = CalculateAliceRank(rankedScores, alice[i]);
            }

            return aliceRanks;
        }

        public static int CalculateAliceRank(List<KeyValuePair<int, int>> rankedScoresAscending, int aliceScore)
        {
            int binarySearchResult = rankedScoresAscending.BinarySearch(new KeyValuePair<int, int>(aliceScore, 0), new RankedScoreComparer());

            int result = 0;

            if(binarySearchResult < 0)
            {
                binarySearchResult = ~binarySearchResult;

                if(binarySearchResult==rankedScoresAscending.Count)
                {
                    result = 1;
                }
                else
                {
                    result = rankedScoresAscending[binarySearchResult].Value + 1;
                }
            }
            else
            {
                result = rankedScoresAscending[binarySearchResult].Value;
            }

            return result;
        }

        public static List<KeyValuePair<int, int>> GetRankedScores(int[] descendingScores)
        {
            int rank = 1;
            var result = new List<KeyValuePair<int, int>>();
            result.Add(new KeyValuePair<int, int>(descendingScores[0], rank));

            for (int i = 1; i < descendingScores.Length; i++)
            {
                if (descendingScores[i - 1] != descendingScores[i])
                {
                    rank++;
                }
                result.Add(new KeyValuePair<int, int>(descendingScores[i], rank));
            }

            return result;
        }

    }
}
