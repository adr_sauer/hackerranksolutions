﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerrankSolutions.Algorithms.AngryProfessor
{
    public class AngryProfessorSolution
    {
        public static string AngryProfessor(int k, int[] a)
        {
            bool classCancelled = true;
            int studentsPresentAtStartTime = 0;

            for (int i = 0; i < a.Length && classCancelled; i++)
            {
                if (a[i] <= 0)
                {
                    studentsPresentAtStartTime++;
                }

                if (studentsPresentAtStartTime >= k)
                {
                    classCancelled = false;
                }
            }


            return classCancelled ? "YES" : "NO";
        }
    }
}
