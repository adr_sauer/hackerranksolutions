﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerrankSolutions.Algorithms.CatsAndAMouse
{
    public class CatsAndAMouseSolution
    {
        public static string CatAndMouse(int x, int y, int z)
        {

            int catADistance = Math.Abs(z - x);
            int catBDistance = Math.Abs(z - y);

            if(catADistance < catBDistance)
            {
                return "Cat A";
            }
            else if(catADistance==catBDistance)
            {
                return "Mouse C";
            }
            else
            {
                return "Cat B";
            }
        }
    }
}
