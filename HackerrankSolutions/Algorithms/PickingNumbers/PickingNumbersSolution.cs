﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerrankSolutions.Algorithms.PickingNumbers
{
    public class PickingNumbersSolution
    {
        public static int PickingNumbers(List<int> a)
        {
            Dictionary<int, int> numberCounts = new Dictionary<int, int>();

            foreach(int number in a)
            {
                if(!numberCounts.ContainsKey(number))
                {
                    numberCounts.Add(number, 0);
                }

                numberCounts[number]++;
            }

            List<KeyValuePair<int,int>> sortedByNumberCounts = numberCounts.OrderBy(q => q.Key).ToList();

            int maxCount = 2;

            for(int i=0;i<sortedByNumberCounts.Count;i++)
            {
                if(sortedByNumberCounts[i].Value > maxCount)
                {
                    maxCount = sortedByNumberCounts[i].Value;
                }

                if(i+1 < sortedByNumberCounts.Count && sortedByNumberCounts[i+1].Key == sortedByNumberCounts[i].Key+1)
                {
                    int sumCount = sortedByNumberCounts[i].Value + sortedByNumberCounts[i + 1].Value;

                    if(sumCount > maxCount)
                    {
                        maxCount = sumCount;
                    }
                }
            }

            return maxCount;
        }
    }
}
