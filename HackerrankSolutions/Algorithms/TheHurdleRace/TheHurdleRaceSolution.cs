﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerrankSolutions.Algorithms.TheHurdleRace
{
    public class TheHurdleRaceSolution
    {
        public static int HurdleRace(int k, int[] height)
        {
            int maxHurdle = height.Max();

            int potionNeeded = 0;

            if (k < maxHurdle)
            {
                potionNeeded = maxHurdle - k;
            }

            return potionNeeded;
        }
    }
}
