﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerrankSolutions.Algorithms.DesignerPdfViewer
{
    public class DesignerPdfViewerSolution
    {
        public static int DesignerPdfViewer(int[] h, string word)
        {
            int maxHeight = 0;

            foreach (char c in word.ToCharArray())
            {
                int height = h[ConvertCharToZeroBasedIndex(c)];

                if (height > maxHeight)
                {
                    maxHeight = height;
                }
            }

            return maxHeight * word.Length;
        }

        public static int ConvertCharToZeroBasedIndex(char c)
        {
            int index = c - 'a';

            return index;
        }
    }
}
