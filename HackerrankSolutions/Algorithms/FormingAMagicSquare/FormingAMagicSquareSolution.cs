﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerrankSolutions.Algorithms.FormingAMagicSquare
{
    public class FormingAMagicSquareSolution
    {
        //Brute force solution - possibility for improvement: only 8 possible magic squares exist

        private const int N = 3;
        private const int magicSum = 15;
        private const int magicSquareSum = 45;

        public static int FormingMagicSquare(int[][] s)
        {
            List<int[][]> allMagicSquares3By3 = CalculateAll3By3MagicSquares();


            int minDistance = int.MaxValue;

            foreach(int[][] magicSquare in allMagicSquares3By3)
            {
                int distance = MatrixDistance(magicSquare, s);
                if(distance < minDistance)
                {
                    minDistance = distance;
                }
            }

            return minDistance;
        }

        public static int MatrixDistance(int[][] a, int[][] b)
        {
            int distance = 0;
            for (int i = 0; i < a.Length;i++)
            {
                for (int j = 0; j < a[i].Length; j++)
                {
                    distance += Math.Abs(a[i][j] - b[i][j]);
                }
            }

                return distance;
        }

        public static List<int[][]> CalculateAll3By3MagicSquares()
        {
            List<int[]> allPermutations = GenerateAllPermutationsOfArray(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 });

            List<int[][]> allMagicSquares3By3 = new List<int[][]>();

            foreach (int[] permutation in allPermutations)
            {
                int[][] s = new int[N][];
                s[0] = new int[] { permutation[0], permutation[1], permutation[2] };
                s[1] = new int[] { permutation[3], permutation[4], permutation[5] };
                s[2] = new int[] { permutation[6], permutation[7], permutation[8] };

                if (IsMagicSquare(s))
                {
                    allMagicSquares3By3.Add(s);
                }
            }

            return allMagicSquares3By3;
        }

        public static List<int[]> GenerateAllPermutationsOfArray(int[] list)
        {
            int x = list.Length - 1;
            List<int[]> permutations = new List<int[]>();
            GetPer(permutations, list, 0, x);
            return permutations;
        }

        private static void Swap(ref int a, ref int b)
        {
            if (a == b) return;

            a ^= b;
            b ^= a;
            a ^= b;
        }

        private static void GetPer(List<int[]> permutations ,int[] list, int k, int m)
        {
            if (k == m)
            {
                int[] c = new int[list.Length];
                list.CopyTo(c, 0);
                permutations.Add(c);
            }
            else
                for (int i = k; i <= m; i++)
                {
                    Swap(ref list[k], ref list[i]);
                    GetPer(permutations, list, k + 1, m);
                    Swap(ref list[k], ref list[i]);
                }
        }


        public static bool IsMagicSquare(int[][] s)
        {
            bool result = true;

            for (int i = 0; i < N; i++)
            {
                if ((s[i][0] + s[i][1] + s[i][2]) != magicSum)
                {
                    result = false;
                }
            }

            for (int i = 0; i < N; i++)
            {
                if ((s[0][i] + s[1][i] + s[2][i]) != magicSum)
                {
                    result = false;
                }
            }

            if ((s[0][0] + s[1][1] + s[2][2]) != magicSum)
            {
                result = false;
            }

            if ((s[2][0] + s[1][1] + s[0][2]) != magicSum)
            {
                result = false;
            }

            return result;
        }
    }
}
