﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerrankSolutions.Algorithms.FindDigits
{
    public class FindDigitsSolution
    {
        public static int FindDigits(int n)
        {
            int divisorDigits = 0;
            int tmp = n;
            while (tmp > 0)
            {
                int digit = tmp % 10;

                if (digit != 0 && n % digit == 0)
                {
                    divisorDigits++;
                }

                tmp -= digit;
                tmp /= 10;
            }            

            return divisorDigits;
        }
    }
}
