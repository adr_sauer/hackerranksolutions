﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerrankSolutions.Algorithms.CircularArrayRotation
{
    public class CircularArrayRotationSolution
    {
        public static int[] CircularArrayRotation(int[] a, int k, int[] queries)
        {
            int[] result = new int[queries.Length];

            for (int i = 0; i < queries.Length; i++)
            {
                result[i] = a[OldIndex(queries[i], a.Length, k)];
            }

            return result;
        }

        private static int OldIndex(int newIndex, int n, int rotations)
        {
            return (newIndex + n - (rotations % n)) % n;
        }
    }
}
