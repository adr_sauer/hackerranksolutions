﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using HackerrankSolutions.Algorithms.AngryProfessor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerrankSolutions.Algorithms.AngryProfessor
{
    [TestClass()]
    public class AngryProfessorSolutionTests
    {
        [TestMethod()]
        public void AngryProfessorTest1()
        {
            int studentsNumberThreshold = 3;
            int[] arrivalTimes = { -1, -3, 4, 2 };


            string result = AngryProfessorSolution.AngryProfessor(studentsNumberThreshold, arrivalTimes);

            Assert.AreEqual("YES", result);
        }


        [TestMethod()]
        public void AngryProfessorTest2()
        {
            int studentsNumberThreshold = 2;
            int[] arrivalTimes = { 0, -1, 2, 1 };


            string result = AngryProfessorSolution.AngryProfessor(studentsNumberThreshold, arrivalTimes);

            Assert.AreEqual("NO", result);
        }
    }
}