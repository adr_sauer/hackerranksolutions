﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using HackerrankSolutions.Algorithms.FormingAMagicSquare;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerrankSolutions.Algorithms.FormingAMagicSquare
{
    [TestClass()]
    public class FormingAMagicSquareSolutionTests
    {
        [TestMethod()]
        public void FormingMagicSquareTest1()
        {
            int[][] s = new int[][]
            {
                new int[] { 4, 9 ,2 },
                new int[] { 3, 5, 7 },
                new int[] { 8, 1 ,5 }
            };

            int expectedResult = 1;

            int result = FormingAMagicSquareSolution.FormingMagicSquare(s);

            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod()]
        public void FormingMagicSquareTest2()
        {
            int[][] s = new int[][]
            {
                new int[] { 4, 8, 2 },
                new int[] { 4, 5, 7 },
                new int[] { 6, 1, 6 }
            };

            int expectedResult = 4;

            int result = FormingAMagicSquareSolution.FormingMagicSquare(s);

            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod()]
        public void IsMagicSquareTest1()
        {
            int[][] s = new int[][]
            {
                new int[] { 4, 9 ,2 },
                new int[] { 3, 5, 7 },
                new int[] { 8, 1 ,5 }
            };

            Assert.IsFalse(FormingAMagicSquareSolution.IsMagicSquare(s));
        }

        [TestMethod()]
        public void IsMagicSquareTest2()
        {
            int[][] s = new int[][]
            {
                new int[] { 4, 9 ,2 },
                new int[] { 3, 5, 7 },
                new int[] { 8, 1 ,6 }
            };

            Assert.IsTrue(FormingAMagicSquareSolution.IsMagicSquare(s));
        }

        [TestMethod()]
        public void CalculateAll3By3MagicSquaresTest()
        {
            FormingAMagicSquareSolution.CalculateAll3By3MagicSquares();
        }

        [TestMethod()]
        public void GenerateAllPermutationsOfArrayTest1()
        {
            int[] permutationElements = new int[] { 1 };

            var result = FormingAMagicSquareSolution.GenerateAllPermutationsOfArray(permutationElements);
            Assert.AreEqual(1, result.Count);
        }

        [TestMethod()]
        public void GenerateAllPermutationsOfArrayTest2()
        {
            int[] permutationElements = new int[] { 1,2 };

            var result = FormingAMagicSquareSolution.GenerateAllPermutationsOfArray(permutationElements);
            Assert.AreEqual(2, result.Count);
        }

        [TestMethod()]
        public void GenerateAllPermutationsOfArrayTest3()
        {
            int[] permutationElements = new int[] { 1,2,3 };

            var result = FormingAMagicSquareSolution.GenerateAllPermutationsOfArray(permutationElements);
            Assert.AreEqual(6, result.Count);
        }

        //[TestMethod()]
        //public void GenerateAllPermutationsOfListTest1()
        //{
        //    List<int> permutationElements = new List<int> { 1 };

        //    var result = FormingAMagicSquareSolution.GenerateAllPermutationsOfList(permutationElements);
        //    Assert.AreEqual(1, result.Count);
        //}



        //[TestMethod()]
        //public void GenerateAllPermutationsOfListTest2()
        //{
        //    List<int> permutationElements = new List<int> { 1,2 };

        //    var result = FormingAMagicSquareSolution.GenerateAllPermutationsOfList(permutationElements);
        //    Assert.AreEqual(2, result.Count);
        //}

        //[TestMethod()]
        //public void GenerateAllPermutationsOfListTest3()
        //{
        //    List<int> permutationElements = new List<int> { 1,2,3 };

        //    var result = FormingAMagicSquareSolution.GenerateAllPermutationsOfList(permutationElements);
        //    Assert.AreEqual(6, result.Count);
        //}
    }
}