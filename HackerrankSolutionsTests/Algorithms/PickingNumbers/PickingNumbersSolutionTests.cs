﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using HackerrankSolutions.Algorithms.PickingNumbers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerrankSolutions.Algorithms.PickingNumbers
{
    [TestClass()]
    public class PickingNumbersSolutionTests
    {
        [TestMethod()]
        public void PickingNumbersTest1()
        {
            List<int> inputArray = new List<int> { 4, 6, 5, 3, 3, 1 };
            int expectedOutput = 3;

            int result = PickingNumbersSolution.PickingNumbers(inputArray);

            Assert.AreEqual(expectedOutput, result);
        }

        [TestMethod()]
        public void PickingNumbersTest2()
        {
            List<int> inputArray = new List<int> { 1, 2, 2, 3, 1, 2 };
            int expectedOutput = 5;

            int result = PickingNumbersSolution.PickingNumbers(inputArray);

            Assert.AreEqual(expectedOutput, result);
        }
    }
}