﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using HackerrankSolutions.Algorithms.ClimbingTheLeaderboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerrankSolutions.Algorithms.ClimbingTheLeaderboard
{
    [TestClass()]
    public class ClimbingTheLeaderboardSolutionTests
    {
        [TestMethod()]
        public void ClimbingLeaderboardTest1()
        {
            int[] scores = { 100, 100, 50, 40, 40, 20, 10 };
            int[] alice = { 5, 25, 50, 120 };
            int[] expectedResult = { 6, 4, 2, 1 };

            int[] result = ClimbingTheLeaderboardSolution.ClimbingLeaderboard(scores, alice);

            Assert.IsTrue(result.SequenceEqual(expectedResult));
        }

        [TestMethod()]
        public void ClimbingLeaderboardTest2()
        {
            int[] scores = { 100, 90, 90, 80, 75, 60 };
            int[] alice = { 50, 65, 77, 90, 102 };
            int[] expectedResult = { 6, 5, 4, 2, 1 };

            int[] result = ClimbingTheLeaderboardSolution.ClimbingLeaderboard(scores, alice);

            Assert.IsTrue(result.SequenceEqual(expectedResult));
        }
    }
}