﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using HackerrankSolutions.Algorithms.TheHurdleRace;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerrankSolutions.Algorithms.TheHurdleRace
{
    [TestClass()]
    public class TheHurdleRaceSolutionTests
    {
        [TestMethod()]
        public void HurdleRaceTest1()
        {
            int naturalJumpAbility = 4;
            int[] hurdles = { 1, 6, 3, 5, 2 };
            int expectedResult = 2;

            int result=TheHurdleRaceSolution.HurdleRace(naturalJumpAbility, hurdles);

            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod()]
        public void HurdleRaceTest2()
        {
            int naturalJumpAbility = 7;
            int[] hurdles = { 2, 5, 4, 5, 2 };
            int expectedResult = 0;

            int result = TheHurdleRaceSolution.HurdleRace(naturalJumpAbility, hurdles);

            Assert.AreEqual(expectedResult, result);
        }
    }
}