﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using HackerrankSolutions.Algorithms.CircularArrayRotation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerrankSolutions.Algorithms.CircularArrayRotation
{
    [TestClass()]
    public class CircularArrayRotationSolutionTests
    {
        [TestMethod()]
        public void CircularArrayRotationTest1()
        {
            int rotationCount = 2;
            int[] array = { 1, 2, 3 };
            int[] queries = { 0, 1, 2 };
            int[] expectedResult = { 2, 3, 1 };

            var result = CircularArrayRotationSolution.CircularArrayRotation(array, rotationCount, queries);

            Assert.IsTrue(expectedResult.SequenceEqual(result));
        }
    }
}