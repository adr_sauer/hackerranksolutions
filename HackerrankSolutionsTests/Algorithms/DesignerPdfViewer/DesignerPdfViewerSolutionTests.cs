﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using HackerrankSolutions.Algorithms.DesignerPdfViewer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerrankSolutions.Algorithms.DesignerPdfViewer
{
    [TestClass()]
    public class DesignerPdfViewerSolutionTests
    {
        [TestMethod()]
        public void DesignerPdfViewerTest1()
        {
            int[] letterHeights = { 1, 3, 1, 3, 1, 4, 1, 3, 2, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 };
            string word = "abc";
            int expectedOutput = 9;

            int result = DesignerPdfViewerSolution.DesignerPdfViewer(letterHeights, word);

            Assert.AreEqual(expectedOutput, result);
        }

        [TestMethod()]
        public void DesignerPdfViewerTest2()
        {
            int[] letterHeights = { 1, 3, 1, 3, 1, 4, 1, 3, 2, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 7 };
            string word = "zaba";
            int expectedOutput = 28;

            int result = DesignerPdfViewerSolution.DesignerPdfViewer(letterHeights, word);

            Assert.AreEqual(expectedOutput, result);
        }
    }
}