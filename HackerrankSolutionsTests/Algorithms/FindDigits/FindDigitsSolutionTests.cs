﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using HackerrankSolutions.Algorithms.FindDigits;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerrankSolutions.Algorithms.FindDigits
{
    [TestClass()]
    public class FindDigitsSolutionTests
    {
        [TestMethod()]
        public void FindDigitsTest1()
        {
            int number = 12;
            int expectedResult = 2;

            var result = FindDigitsSolution.FindDigits(number);

            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod()]
        public void FindDigitsTest2()
        {
            int number = 1012;
            int expectedResult = 3;

            var result = FindDigitsSolution.FindDigits(number);

            Assert.AreEqual(expectedResult, result);
        }


        [TestMethod()]
        public void FindDigitsTest3()
        {
            int number = 4;
            int expectedResult = 1;

            var result = FindDigitsSolution.FindDigits(number);

            Assert.AreEqual(expectedResult, result);
        }
    }
}