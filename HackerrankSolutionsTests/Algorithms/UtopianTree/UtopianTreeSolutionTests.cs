﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using HackerrankSolutions.Algorithms.UtopianTree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerrankSolutions.Algorithms.UtopianTree
{
    [TestClass()]
    public class UtopianTreeSolutionTests
    {
        [TestMethod()]
        public void UtopianTreeTest1()
        {
            int numberOfCycles = 0;
            int expectedResult = 1;

            var result = UtopianTreeSolution.UtopianTree(numberOfCycles);

            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod()]
        public void UtopianTreeTest2()
        {
            int numberOfCycles = 1;
            int expectedResult = 2;

            var result = UtopianTreeSolution.UtopianTree(numberOfCycles);

            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod()]
        public void UtopianTreeTest3()
        {
            int numberOfCycles = 4;
            int expectedResult = 7;

            var result = UtopianTreeSolution.UtopianTree(numberOfCycles);

            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod()]
        public void UtopianTreeTest4()
        {
            int numberOfCycles = 2;
            int expectedResult = 3;

            var result = UtopianTreeSolution.UtopianTree(numberOfCycles);

            Assert.AreEqual(expectedResult, result);
        }
    }
}