﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using HackerrankSolutions.Algorithms.CatsAndAMouse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerrankSolutions.Algorithms.CatsAndAMouse
{
    [TestClass()]
    public class CatsAndAMouseSolutionTests
    {
        [TestMethod()]
        public void CatAndMouseTest1()
        {
            int catA = 1;
            int catB = 2;
            int mouseC = 3;
            string expectedResult = "Cat B";

            string result = CatsAndAMouseSolution.CatAndMouse(catA, catB, mouseC);

            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod()]
        public void CatAndMouseTest2()
        {
            int catA = 1;
            int catB = 3;
            int mouseC = 2;
            string expectedResult = "Mouse C";

            string result = CatsAndAMouseSolution.CatAndMouse(catA, catB, mouseC);

            Assert.AreEqual(expectedResult, result);
        }
    }
}