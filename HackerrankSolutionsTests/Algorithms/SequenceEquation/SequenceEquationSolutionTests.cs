﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using HackerrankSolutions.Algorithms.SequenceEquation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerrankSolutions.Algorithms.SequenceEquation
{
    [TestClass()]
    public class SequenceEquationSolutionTests
    {
        [TestMethod()]
        public void PermutationEquationTest1()
        {
            int[] p = { 2,3,1 };
            int[] expectedResult = { 2,3,1 };

            var result = SequenceEquationSolution.PermutationEquation(p);

            Assert.IsTrue(expectedResult.SequenceEqual(result));
        }

        [TestMethod()]
        public void PermutationEquationTest2()
        {
            int[] p = { 4, 3, 5, 1, 2 };
            int[] expectedResult = { 1, 3, 5, 4, 2 };

            var result = SequenceEquationSolution.PermutationEquation(p);

            Assert.IsTrue(expectedResult.SequenceEqual(result));
        }
    }
}